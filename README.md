# gitlab-tools

See [the wiki](https://gitlab.com/someguy20336/gitlab-tools/-/wikis/home) for building, testing, and deploying instructions.

## Hiding resolved discussions

This extensions automatically hides resolved discussions in issues and merge requests and allows you to toggle them via a checkbox.  


