import { BaseResolvedThreadHandler, hideResolvedClass } from "./BaseResolvedThreadHandler";
import { findNotesAndInit, logDebug } from "./utils";

class MergeRequestResolvedThreadHandler extends BaseResolvedThreadHandler {

	private failureCount = 0;

	constructor(el: HTMLElement) {
		super(el);
	}

	protected ensureMounted(cntForRetry: number) {
		if (this.toggleContainers.length > 0) {
			return;
		}

		logDebug("Adding resolved count button");
		// Find the element to inject the "Show X Resolved" next to
		// This is the element as of GitLab 15.
		// It also now shows up in 2 spots
		let resolveCntContainers = document.querySelectorAll(".discussions-counter");

		if (resolveCntContainers.length >= 2 || (resolveCntContainers.length === 1 && this.failureCount > 6)) {
			for (const oneResolveCnt of resolveCntContainers) {
				const newToggleContainer = this.createNewNode();
				oneResolveCnt.prepend(newToggleContainer);
			}
			logDebug(`Found ${resolveCntContainers.length} containers and appended the button`);
			this.failureCount = 0;
		} else {				
			logDebug("Failed to find container for show resolved toggle");
			this.failureCount++;

			if (this.failureCount < 10) {
				logDebug("Trying again in 1 second");
				setTimeout(() => {
					this.updateResolvedCnt(cntForRetry);
				}, 1000);
			} else {					
				logDebug("Failed too many times, showing all discussions now");
				this.notesListEl.classList.remove(hideResolvedClass);	// Ensure we don't hide on too many failures
			}
		
		}
	}
}

findNotesAndInit((el) => {
	const handler = new MergeRequestResolvedThreadHandler(el);
	handler.init();
});