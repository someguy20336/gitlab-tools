const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
module.exports = {
    // TODO: a dev mode for dev?  would be nice, but whatever
   mode: "production",
   entry: {
      "merge-request": path.resolve(__dirname, "merge-request.ts"),
      "issue": path.resolve(__dirname, "issue.ts"),
   },
   output: {
      path: path.join(__dirname, "../dist"),
      filename: "[name].js",
   },
   resolve: {
      extensions: [".ts", ".js"],
   },
   module: {
      rules: [
         {
            test: /\.tsx?$/,
            loader: "ts-loader",
            exclude: /node_modules/,
         },
      ],
   },
   plugins: [
      new CopyPlugin({
         patterns: [
            {from: "*.css", to: "../dist"},
            {from: "manifest.json", to: "../dist"},
            {from: "icons", to: "../dist/icons"}
        ]
      }),
   ],
};