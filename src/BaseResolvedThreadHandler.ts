import { logDebug } from "./utils";

export const hideResolvedClass = "hide-resolved";

export abstract class BaseResolvedThreadHandler {

	protected notesListEl: HTMLElement;
	protected toggleContainers: HTMLDivElement[] = [];

	constructor(el: HTMLElement) {
		this.notesListEl = el;
	}

    public init() {
			
		logDebug("Initializing Gitlab Resolved extension");

		if (location.hash.includes("note_")) {
			this.findResolved(true);
		} else {
			this.toggleState();  // starts in "hidden" state
		}

		// Create an observer instance linked to the callback function
		const observer = new MutationObserver(() => this.findResolved(true));

		// Start observing the target node for configured mutations
		observer.observe(this.notesListEl, { childList: true });

		// Look for button clicks for resolve/unresolve
		this.notesListEl.addEventListener("click", (ev) => {
			var btn = (ev.target as HTMLElement)?.closest("button");
            
			if (btn !== null) {
				// Hack to account for the network delay
				setTimeout(() => {				
					var val = btn!.dataset.testid;
					if (val === "resolve-discussion-button") {
						this.findResolved(false);
					}
				}, 3000);
			}
			
		});
    }

	
	private toggleState() {
		this.findResolved(true);
		const wasAdded = this.notesListEl?.classList.toggle(hideResolvedClass);
		for (const container of this.toggleContainers) {
			const chkbox = container.querySelector("input")!;
			chkbox.checked = !wasAdded;
		}
	}
	
	// Refresh the resolved notes
	private findResolved(updateClasses: boolean) {
		let notes = this.notesListEl!.querySelectorAll(".note-discussion");
		
		logDebug(`Found ${notes.length} note-discussion elements`);
		
		let cnt = 0;
		for (let oneNote of notes) {
			let headline = oneNote.querySelector(".js-discussion-headline");
			let isResolved = false;

			// Actually seems like the presence of this is good, but lets just check text
			// just in case
			if (headline !== null) {
				if (headline.textContent?.trim().startsWith("Resolved")) {
					cnt++;
					isResolved = true;
				}
			}
			
			if (updateClasses) {
				oneNote.classList.toggle("hf-resolved", isResolved);
			}
		}
		
		this.updateResolvedCnt(cnt);
		logDebug(`Found ${cnt} resolved discussions`);
	}

	protected updateResolvedCnt(cnt: number) {		
		this.ensureMounted(cnt);
		for (const container of this.toggleContainers) {
			const lbl = container.querySelector("label")!;
			lbl.innerText = `Show ${cnt} resolved`;
		}
	};

	protected abstract ensureMounted(cntForRetry: number): void;

	protected createNewNode() {
		// Checkbox
		const chkToggleResolve = document.createElement("input");
		chkToggleResolve.type = "checkbox";
		chkToggleResolve.id = "show-resolved-threads";
		chkToggleResolve.checked = !this.notesListEl.classList.contains(hideResolvedClass)
		chkToggleResolve.addEventListener("change", () => this.toggleState());

		// Label
		const lblToggleResolve = document.createElement("label");
		lblToggleResolve.innerText = `Show 0 resolved`;
		lblToggleResolve.setAttribute("for", "show-resolved-threads");

		// ToggleContainer
		const toggleContainer = document.createElement("div");
		toggleContainer.classList.add("line-resolve-all", "gl-flex", "gl-items-center", "gl-pl-4", "gl-rounded-base", "gl-mr-3", "gl-bg-gray-50", "gl-pr-4");
		toggleContainer.appendChild(chkToggleResolve);
		toggleContainer.appendChild(lblToggleResolve);

		this.toggleContainers.push(toggleContainer);
		return toggleContainer;
	}
}
