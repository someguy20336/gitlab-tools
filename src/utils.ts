
// Logs a debug message
let debug = false;
export function logDebug(msg: string) {	
	if (debug) {
		console.log(msg);
	}
};

export function findNotesAndInit(intializer: (el: HTMLElement) => void) {
	// Find the notes list
	let el = document.getElementById("notes-list");

	if (el === null) {		
		logDebug("Failed to find notes-list.  Rretrying in 5 seconds.");
		setTimeout(() => {
			findNotesAndInit(intializer);
		}, 5000);
	} else {
		intializer(el);
	}
}