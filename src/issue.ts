import { BaseResolvedThreadHandler, hideResolvedClass } from "./BaseResolvedThreadHandler";
import { findNotesAndInit, logDebug } from "./utils";

class IssueResolvedThreadHandler extends BaseResolvedThreadHandler {

	private failureCount = 0;

	constructor(el: HTMLElement) {
		super(el);
	}

	protected ensureMounted(cntForRetry: number) {
		if (this.toggleContainers.length > 0) {
			return;
		}

		logDebug("Adding resolved count button");
		// Find the element to inject the "Show X Resolved" next to
		let prefElement = document.getElementById("discussion-preferences");

		if (prefElement !== null) {
            const newToggleContainer = this.createNewNode();
            prefElement.parentElement!.prepend(newToggleContainer);
			
			logDebug(`Found target and successfully appended the button`);
			this.failureCount = 0;
		} else {				
			logDebug("Failed to find container for show resolved toggle");
			this.failureCount++;

			if (this.failureCount < 10) {
				logDebug("Trying again in 1 second");
				setTimeout(() => {
					this.updateResolvedCnt(cntForRetry);
				}, 1000);
			} else {					
				logDebug("Failed too many times, showing all discussions now");
				this.notesListEl.classList.remove(hideResolvedClass);	// Ensure we don't hide on too many failures
			}
		
		}
	}
}

findNotesAndInit((el) => {
	const handler = new IssueResolvedThreadHandler(el);
	handler.init();
});